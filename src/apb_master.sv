/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2020-2023 MuntasirBhai, Inc (All Rights Reserved)
//
// Project Name: APB
// Module Name:  apb_master
// Designer:     Muntasir
// Description:  This is component is apb master which recieves requests from
//               AXI transaction and sends apb transaction
//
// MuntasirBhai, Inc Proprietary and Confidential
/////////////////////////////////////////////////////////////////////////////////

`include "apb_master_pkg.sv"

module apb_m
  import apb_master_pkg::ADDR_WIDTH;
  import apb_master_pkg::DATA_WIDTH;
  import apb_master_pkg::apb_req_s;
  import apb_master_pkg::apb_resp_s;

#()(
  // APB interface
  input   logic                     clk_i,         // CLock
  input   logic                     arst_n,      // reset
  output  logic [ADDR_WIDTH-1:0]    paddr_o,      // Address
  output  logic                     psel_o,       // slave select
  output  logic                     penable_o,    // 
  output  logic                     pwrite_o,     // 1:write, 0:read
  output  logic [DATA_WIDTH-1:0]    pwdata_o,     // write data
  output  logic [DATA_WIDTH/8-1:0]  pstrb_o,      // for write only, indicates byte lane
  input   logic                     pready_i,     // ready from slave
  input   logic [DATA_WIDTH-1:0]    prdata_i,     // read data
  input   logic                     pslverr_i,    // slave error

  // AXI to APB
  input   apb_req_s                 apb_req_i,
  // APB to AXI
  output  apb_resp_s                apb_resp_o  
);

logic [31:0] paddr_q;

logic [3:0] count, next_count;

typedef enum logic [2:0] { 
  IDLE    = 3'b000, 
  SETUP   = 3'b010, 
  ACCESS  = 3'b100
} apb_state_t;

apb_state_t state;
apb_state_t next_state;

always_ff @(posedge clk_i or negedge arst_n)
begin 
  if(!arst_n)
    state <= IDLE;
  else
    state <= next_state;
end

// APB Master state machine
always_comb
begin 
    case(state)
    
    IDLE: 
    begin 

      if(apb_req_i.sel==1) 
      begin 
        next_state = SETUP;
        
      end
      else
      begin 
        next_state = IDLE;
      end
    end
    
    SETUP:
    begin 
      next_state = ACCESS;
    end

    ACCESS:
    begin 
      if(pready_i)
        next_state = IDLE;
      else
        next_state = ACCESS;
    end

    default: next_state = IDLE;
    endcase
end
  
// APB PADDR
always_ff@(posedge clk_i or negedge arst_n)
  if(!arst_n)
    paddr_q <= '0;
  else 
    paddr_q <= apb_req_i.addr;

always_ff@(posedge clk_i or negedge arst_n)
  if(!arst_n)
    pwdata_o <= '0;
  else
    pwdata_o <= apb_req_i.data;

// APB PSTRB
always_ff@(posedge clk_i or negedge arst_n)
  if(!arst_n)
    pstrb_i <= '0;
  else
    pstrb_i <= apb_req_i.strb;     

// Output Assignments
assign psel_o     = (state == SETUP) | (state == ACCESS);
assign penable_o  = (state == ACCESS);
assign paddr_o    = paddr_q & {32{state == ACCESS}};
assign pwrite_o   = apb_req_i.we;


endmodule