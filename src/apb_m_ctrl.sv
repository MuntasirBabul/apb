/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2020-2023 MuntasirBhai, Inc (All Rights Reserved)
//
// Project Name: APB
// Module Name:  apb_m_ctrl
// Designer:     Muntasir
// Description:  apb_m_ctrl is the controller for apb master. This controller 
//               AXI bus interface and will send transaction to drive apb master
//
// MuntasirBhai, Inc Proprietary and Confidential
/////////////////////////////////////////////////////////////////////////////////

module apb_m_ctrl();
endmodule