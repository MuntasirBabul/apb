package apb_master_pkg;

    parameter ADDR_WIDTH = 32;
    parameter DATA_WIDTH = 32;

    typedef struct packed {
        logic [ADDR_WIDTH-1:0] addr;
        logic [DATA_WIDTH-1:0] data;
        logic                  sel;
        logic                  strb;
        logic                  we;
    } apb_req_s;

    typedef struct packed {
        logic [ADDR_WIDTH-1:0] addr;
        logic [DATA_WIDTH-1:0] data;
    } apb_resp_s; // not done

endpackage

