/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2020-2023 MuntasirBhai, Inc (All Rights Reserved)
//
// Project Name: APB
// Module Name:  apb_slave
// Designer:     Muntasir
// Description:  Its just a fucking memory which supports apb transaction
//
// MuntasirBhai, Inc Proprietary and Confidential
/////////////////////////////////////////////////////////////////////////////////

module apb_slave
#(
	parameter WIDTH = 8,
	parameter DEPTH = 100,
	parameter ID    = 0

) (
  input  logic    	      pclk_i,
  input  logic    	      presetn_i,
  input  logic    [31:0]  paddr_i,
  input  logic    	      psel_i,
  input  logic    	      penable_i,
  input  logic    	      pwrite_i,
  input  logic    [31:0]  pwdata_i,
  input  logic    [3:0 ]  pstrb_i,
  output logic    [31:0]  prdata_o,
  output logic    	      pready_o
);
//////////////////////////////////////////////////////////////////////////////
// Local Parameters
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Signals
//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////
    
  logic[WIDTH-1:0] mem [DEPTH-1:0];

  always_ff@(posedge pclk_i )
  begin
    if(!presetn_i) 
    begin
      pready_o <=1;
      prdata_o <=0;
    end  
    else 
    begin
      pready_o<=1;    
      if(psel_i==ID && penable_i) 
      begin
        pready_o<=0;    
        if(pwrite_i) 
        begin
          for(int i=0; i<4;i++) 
          begin
            if(pstrb_i[i])
              mem[paddr_i+i] <=pwdata_i[(8*(i+1)-1)-:8];
          end   
        end	    
        else 
        begin
        for(int i=0; i<4;i++)
          prdata_o[(8*(i+1)-1)-:8] <= mem[paddr_i+i];
        end
      end
    end
  end
endmodule