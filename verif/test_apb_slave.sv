`include "../src/apb_master_pkg.sv"
import apb_master_pkg::ADDR_WIDTH;
import apb_master_pkg::DATA_WIDTH;

module test_apb();

localparam WIDTH = 8;
localparam DEPTH = 100;
localparam ID    = 0;

bit clk;
bit arst_n;
logic [DATA_WIDTH-1:0] read_data;
logic ready;

typedef struct packed {
  logic [ADDR_WIDTH-1:0]   addr;
  logic                    sel;
  logic                    en;
  logic                    wr_en;
  logic [DATA_WIDTH-1:0]   write_data;
  logic [DATA_WIDTH/8-1:0] strb;
} apb_req_s;

apb_req_s req;

apb_slave slave (
  .pclk_i      (clk),
  .presetn_i   (arst_n),
  .paddr_i     (addr),
  .psel_i      (sel),
  .penable_i   (en),
  .pwrite_i    (wr_en),
  .pwdata_i    (write_data),
  .pstrb_i     (strb),
  .prdata_o    (read_data),
  .pready_o    (ready)
);

task apply_clk();
  forever #5 clk = ~clk;
endtask

task apply_reset();
  arst_n = '1;
  clk    = '1;
  #10;
  arst_n = '0;
  #10;
  arst_n = '1;
endtask

initial begin
  apply_reset();
  apply_clk();
  #100;
  $finish;
end

initial begin 
  $dumpfile("dump.vcd");
  $dumpvars;
  $finish;
end

endmodule